Commencer avec GIT
=====================


Débuter avec GIT
----------------

Avant d'aller lire de la documentation, les points essentiels à retenir d'un point de vue utilisation sont :

- l'aspect distribué de Git : chaque développeur (et chaque serveur) a un repo complet local. 
- le serveur de référence (par convention appelé _origin_) est donc un repo parmi les autres.
- les tags et les branches ne pèsent rien !  Si vous êtes habitués aux branches svn, oubliez-les. Une branche sous GIT n'est qu'une référence vers un commit, et ne pèse donc rien, c'est instantané et d'un cout négligeable. Un tag étant une association de nom à une référence de commit, un tag ne coute pas plus cher ! Et comme le repo est local à votre machine, vous pouvez créer vos branches et vos tags localement, simplement pour faciliter votre organisation et votre flot. 


---------------------------------------------------------------------

Première prise en main
----------------------

Niveau interface graphique (GUI), [Sourcetree](https://www.atlassian.com/software/sourcetree/overview) développé par Atlassian est très pratique au quotidien, et disponible gratuitement pour Windows et Mac. Pour certaines fonctions avancées le terminal est toujours là, et pour les add/commit/fetch/pull/merge quotidiens, Sourcetree rend le flot pas mal convivial.
Sourcetree peut inclure une copie de Git (si vous choisissez cette option), sinon des clients Git existent pour toutes les plateformes, telles que [msysgit](http://msysgit.github.io/) sous Windows.


Pour faire vos premiers pas, la lecture du tutorial [Git for absolute beginners](http://rogerdudler.github.com/git-guide/) est parfait pour comprendre ce qui se passe et comment est organisé le flot. Tout y est expliqué avec les lignes de commandes... c'est toujours utile à mémoriser, mais sans forcément retenir la syntaxe précise des arguments, retenez les noms des commandes usuelles, car les interfaces graphiques utiliseront de toute façon ces noms. Toutes les manipulations de ce tutoriel pourront être faites de façon simple via Sourcetree. Retenez le flot, la logique et le nom des commandes, et laissez l'interface vous guider pour les premiers pas (vous pourrez toujours faire des relectures une fois acclimatés pour avoir plus d'options et de contrôle).


Pour les habitués de SVN, une des principales différences viendra dans le flot : lorsque vous commitez, votre commit est toujours local (puisque vous avez un repo local). Et ce qui correspond au commit vers un serveur distant sous SVN est apporté dans Git par le push (pour pousser les modifs de notre branche locale vers le serveur distant).

Enfin, n'oubliez pas que votre repo est local. Aussi vous êtes encouragés à commiter très souvent, en local, pour avoir des possibilités d'historisation et de rollback illimitées.
Vous pouvez même faire un fork de votre origin pour avoir un deuxième remote, que vous utilisez pour pousser vos commits en attendant d'être stable. Une fois votre code stable, poussez vos commit sur le remote _origin_ que vous partagez avec votre équipe. Ainsi origin a toujours une copie stable du projet, et votre fork vous permet d'avoir en permanence un backup en cas de crash majeur.
Enfin n'oubliez pas qu'en local, vous pouvez faire des branches et des tags à volonté. Évitez simplement de pousser vos tags et branches locaux et temporaires vers l'origin, pour ne pas polluer les repos de vos collègues.

---------------------------------------------------------------------

Flot de travail habituel
-----------------------------
	
Ne *JAMAIS* forcer un push vers un remote.
Si Git refuse le push de vos modifications, c'est que quelque chose ne va pas. Demandez de l'aide pour d'abord fixer localement votre problème et pouvoir pousser vos modifs.

Le flot habituel de travail est le suivant :

- origin/master correspond aux versions finales/releases, tout le travail se fait sur la branche develop (par convention)
- pull du origin/develop pour avoir une copie locale de develop
- créer une branche locale de develop et travailler dessus
- travailler, tester, commiter, travailler, tester, commiter, ...
- (optionnel) si d'autres dév travaillent sur la même feature que vous, poussez la branche sur origin
- une fois votre feature stable, lorsque vous souhaitez le passer sur develop :
- repassez sur develop (checkout), faites un fetch/pull pour être sûr que votre develop local est à jour avec origin/develop
- mergez votre branche locale dans votre branche develop locale
- testez ! (et retestez encore pour être sûr)
- faites un push de votre branche develop vers `origin/develop`
- effacez votre branche locale (et la branche distante aussi, si vous l'aviez poussée)



---------------------------------------------------------------------

Configurer votre environnement
-----------------------------

Le fichier `.gitignore` est pratique pour ignorer les fichiers à ne pas versionner (nous reviendrons dessus), mais le remplir à chaque fois pour chaque repos, est fastidieux et sujet à erreur. Git vous permet d'avoir une configuration globale, aussi nous vous conseillons de vous préparer un fichier `.global_ignore` global et de l'inclure dans votre configuration, et  d'avoir ensuite l'esprit tranquille.
Vous pouvez télécharger un exemple de fichier global_ignore accompagnant cette documentation, et l'ajuster à la nature de vos projets. (Dans le dossier [sample-global-ignore](https://bitbucket.org/sportebois/git-introduction/src/develop/sample-global-ignore) de ce repo)
Pour définir votre ignore global, soit vous consultez les préférences de votre GUI, soit vous utilisez la commande (ici le fichier s'appelle gitignore_global)
```
$ git config --global core.excludesfile ~/.gitignore_global
```
Pour ajuster votre config à vos préférences, vous pouvez vous référer à [l'aide sur l'ignore de Github](https://help.github.com/articles/ignoring-files).


Si vous avez des doutes sur une commande, des options, le terminal vous ouvrira automatiquement l'aide appropriée si vous ajoutez le flag `--help`, par exemple `$ git add --help` ouvrira l'aide détaillée de la commande add. 
Toute la documentation complète est disponible sur le [site officiel de GIT](http://git-scm.com/)


---------------------------------------------------------------------


Différences entre ignorer le versionning et ignorer les modifications
---------------------------------------------------------------------

Le fichier gitignore (local au projet, à un dossier ou global à votre config de Git) est interrogé par Git au moment de traiter un fichier qui n'est pas dans l'index, et qu'il se pose donc la question « dois-je versionner ce fichier ? ».
La config via `core.excludefile` (global) ou `.git/info/exclude` (local à un repo) fonctionne de la même manière que le gitignore : ces informations sont utilisées lorsqu'un nouveau fichier est présenté à Git et qu'il doit décider s'il doit proposer de le versionner ou pas.

La différence entre gitignore et les config d'exclude est ici : gitignore est un fichier du repo, et pourra donc être commité dans le repo, alors que les excludes sont définitivement locaux et ne sont pas partagés avec les remotes.

Une fois qu'un fichier est versionné, le gitignore et les exclude ne le concernent plus. Un autre mécanisme (qui n'existe pas dans SVN) est disponible : le flag `assume-unchanged` sur l'index. Ce flag vous permet de dire à votre repo d'ignorer les modifications sur ce fichier. Ainsi il ne vous proposera pas de commiter vos modifications. Ça peut être très pratique dans une équipe organisée pour inclure des fichiers de configuration 'template' dans le repo, qui permettent de très rapidement importer le projet, mais que chacun conserve ensuite en local pour ne pas écraser la config de ses collègues avec sa config locale.

Par exemple pour récupérer un projet actionscript, si l'équipe est organisée pour respecter ce assume-unchanged, les fichiers de configuration du projet pourront être inclus dans le repo pour permettre sa configuration rapide :
```
$ git clone https://bitbucket.org/myaccount/my-projects.git
$ git update-index --assume-unchanged as3/projectname/.actionScriptProperties
```
Et voilà, vous avez cloné le repo et immédiatement ensuite vous ignorez les modifications sur le fichier de config du projet. Tellement plus simple de mettre à jour deux chemins dans le fichier de propriétés que de recréer tout le projet avec toutes ses dépendances !


---------------------------------------------------------------------

Utiliser le stash comme espace de sauvegarde
--------------------------------------------

Le stash est une fonctionnalité de Git dont vous pouvez totalement vous passer, mais dont on a du mal à se passer une fois qu'on a commencé !
Pensez au stash comme à une 4ème dimension parallèle dans laquelle vous pouvez envoyer vos modifications. Résumé comme ça, ce n'est peut-être pas très rassurant, mais l'idée se résume bien à ça : le stash est un espace à part du working directory, dans lequel vous pouvez demander à git de sauvegarder (avec un nom pour le retrouver plus tard) un jeu de modifs.
Dans la pratique, quand vous voulez nettoyer votre working directory, par exemple pour faire un merge, basculer sur une autre branche, revenir en arrière ou tout autre raison, vous envoyez vos fichiers dans le stash, vous faites ce que vous avez à faire, puis vous pouvez les retrouver plus tard. Si c'est un peu flou, n'hésitez pas à consulter la [cheat-sheet visuelle](http://ndpsoftware.com/git-cheatsheet.html#loc=stash) pour bien voir le flot entre stash et working directory. À ce moment, Sourcetree vous listera automatiquement le(s) stash(es) du repo en cours en dessous des branches et des remotes. Pour réappliquer les modifications conservées, il suffit de faire un clic droit sur votre stash et l'appliquer (sur le working directory en cours donc, peu importe sur quel commit est votre HEAD).


---------------------------------------------------------------------

Bonnes pratiques
----------------

Ce qui suit est issu du bon sens et de l'excellent article [Git best pratices](http://sethrobertson.github.io/GitBestPractices/). 

### Commentez vos commits
C'est évident, c'est déjà recommandé pour tous les SCM comme SVN et autres, mais comme Git vous encourage à faire beaucoup de petits commits, comme vous prendrez l'habitude de parcourir souvent les commits des autres, essayez d'apporter un minimum d'information dans vos messages. 
Pour la lisibilité des logs il est souvent recommandé de faire des lignes de 72 caractères ou moins. Adoptez aussi une convention pour préfixer vos commits et rapidement repérer les ajouts, les bugfixs, le refactoring, ...  Si vos messages ressemblent à ceux de <http://whatthecommit.com/> , c'est qu'il vous reste du travail !


### Ne paniquez pas !
Une fois vos fichiers versionnez, localement (oui nous nous répétons), vous ne pouvez pas les perdre sauf si vous le demandez expressément. Selon ce que vous avez cassé, la récupération sera plus ou moins intuitive, mais vos versions sont toujours toutes conservées. (lisez l'[article](http://sethrobertson.github.io/GitBestPractices/#panic) pour plus de détails sur les manipulations. 

### *Commitez* *souvent*, et beaucoup ! 
Vos commits sont locaux, tant que vous ne poussez pas vers un remote, les bétises que vous faites dans vos commits ne perturbent personne, et ces nombreux commits vous permettront de facilement revenir en arrière en cas de soucis.

### Harmonisez votre workflow.
Selon les équipes, l'organisation des branches pourra varier, mais le plus important est de vous mettre d'accord sur votre flot. Il ne sera probablement pas parfait dès le premier projet, mais affinez le progressivement. Il existe beaucoup de littérature sur le sujet, mais commence simplement en vous fixant sur l'origin une branche stable (master) et une branche de travail (develop) par exemple. Ceci ne vous empêche pas de créer de nombreuses branche temporaires ou de tags sur votre repo local, mais assurez vous de définir des conventions sur les branches et tags qui pourront être poussés sur l'origin, afin que tout le monde conserve une référence saine et s'y retrouve.

### Préférez de nombreux petits repos plutôt qu'un gros. 
Cela vous permettra de mieux contrôler les forks, les accès, puisqu'ils se font par repo. Aussi n'oubliez pas qu'une branche concerne un repo complet, aussi au plus vous mélangez vos affaires dans un repo, au plus vous aurez envie de ceci de la branche A et ceci de la branche B. 
Si votre projet ne peut pas facilement être organisé en plusieurs repos ou en lib, vous avez des techniques plus avancées avec les submodules, les subtree ou gitslave. Sachez que ça existe, mais essayez de ne pas en avoir besoin trop vite pour ne les aborder qu'une fois familier avec Git.



---------------------------------------------------------------------

Trucs et astuces
----------------

Certaines opérations sont moins intuitives que d'autres, aussi voici un petit florilège d'astuces.
La plupart de ces astuces ne sont pas forcément réalisables avec une IDE comme Sourcetree ou le client Github.


### Ignorer les modifications d'un fichier non ignoré

Très pratique pour partager des fichiers de config, les inclure au repo pour permettre à n'importe quel nouveau venu d'avoir un projet complet, mais pour ne pas pousser sur le repo distant les modifications faites localement spécifiques à notre poste!
```
$ git update-index --assume-unchanged <FILENAME>
```
Si pour un merge, un de ces fichiers pose soucis car une version modifiée à été poussée (à tort ou à raison), vous devez déflager ce fichier. 
``` 
$ git update-index --no-assume-unchanged <FILENAME>
```
Là, selon vos modifications et celles de la branche à merger, vous pouvez au choix discarder vos changements, les mettre en stash pour éviter tout conflit mais les conserver à portée de main (cf. la documentation sur les stash), ou régler le conflit lors du merge.



### Ne pas ignorer un fichier filtré par le gitignore global

Votre liste d'ignore globale sert à vous simplifier la vie. Mais que faire si elle vous fait gagner beaucoup de temps pour 99% de vos projets, mais qu'un projet demande de versionner un fichier ignoré partout ailleurs ? Si par exemple pour un repo vous devez (pour une raison valide) versionner le dossier bin, mais que votre fichier d'ignore global contient, entre-autres :
```
# Build and Release Folders
[Bb]in/
[Bb]in-*/
[Bb]in-*/**
[Bb]in-*/**/*
```
Vous pouvez supprimer ces lignes de votre ignore global, mais vous devriez les ajouter dans les 99% des autres projets ?!
Soyons plus fins que ça. Vous pouvez forcer Git à ajouter des fichiers à l'index malgré le git ignore !
Pour ce faire, vous devez appeler 
```
$ git add --force <FILES>
```
Dans notre exemple, pour ajouter tout le contenu du dossier bin/ nous aurons donc
```$ git add --force bin/\*```

Le git add --force fonctionne bien entendu également avec un gitignore local (mais dans ce cas, il sera plus souvent pertinent de spécifier l'exclusion de l'ignore dans le gitignore avec un ! comme préfixe, consultez la doc du gitignore pour plus de détails)


### Récupérer un fichier dans branche sans merger cette branche
On travaille sur une feature, on revient sur la branch de développement commune, et on voudrait récupérer un fichier de la feature en cours mais on ne veut pas merger les branches... on peut, et le merge ultérieur se fera sans conflit !
```
$ git checkout <BRANCH> -- path/to/file.ext
```


### Supprimer un tag
Comment supprimer un tag sur le repo distant quand on l'a pushé par erreur ?
Les tags locaux étant tellement pratiques qu'on pourrait avoir tendance à en utiliser beaucoup ;-) ... 
La syntaxe de git pour supprimer les branches ou les tags est concise mais obscure :
```
$ git push origin :labelDuTagAEffacer
```


### Ramener notre projet à un ancien commit au choix

L'idée est donc de ramener la référence HEAD sur un commit (vous aurez peut être un avertissement de HEAD flottant au milieu de cette opération), puis de ramener votre projet à un état stable à ce commit.
Étape par étape :
```
# reset back to the desired tree
$ git reset 34e02cdcf

# move the pointer on your branch to the previous HEAD
$ git reset --soft HEAD@{1}
$ git commit -m "Revert to 34e02cdcf"

# Update your working copy to reflect the latest commit
$ git reset --hard
```


### Restaurer un fichier (et un seul) à partir d'un ancien commit

Cette procédure est beaucoup plus simple avec SourceTree qui permet de facilement trouver le commit qui nous intéresse et agrège les commandes Git, donc la procédure sera indiquée avec l'IDE:
Procédure via SourceTree :

0. Vérifiez que votre repo est clean : tout est commité ou stashé
1. Dans votre repo, cherchez le fichier en question (via log/history ou via search)
2. Faites un clic-droit dessus » "Log selected..." pour ouvrir le log de ce fichier uniquement
3. Via l'historique, cherchez le commit en particulier qui contient la version que vous souhaitez restaurer (Cet historique est le gros point intéressant de la restauration via sourceTree avec les vues de diff synthétiques)
4. Une fois le commit intéressant trouvé, faites un clic droit dessus (toujours dans la vue du log du fichier) et sélectionnez "Reset to commit..."   Cette action reviendra à ce commit uniquement pour ce fichier (même si le commit en question avait modifié d'autres fichiers)

Et voilà, votre index a désormais la version antérieure du fichier. 


### Ne pas polluer un gitignore 

Les fichiers gitignore versionné doivent références des exclusions communes à tous les développeurs.
Si pour une raison quelconque vous souhaitez ignorer des éléments spécifiques à votre configuration, ne les ajoutez pas dans un gitignore versionné, mais plutôt dans le fichier `.git/info/exclude` qui est dédié à ça.
Quand vous faites cette manip, posez vous bien la question : ces fichiers ignorés sont-ils bien à leur place dans le repo ?

Plus d'infos dans [la documentation de gitignore](http://git-scm.com/docs/gitignore)


### Utiliser un deuxième remote  pour backuper vos travaux temporaires et instables

Le remote origin ne devrait contenir que des sources intéressantes, et idéalement que des sources qui compilent sans erreurs. Cette condition est systématique pour la branche develop, vous ne devriez jamais commiter de fichier qui bloque l'utilisation de la branch develop. 
Lorsque vous travaillez, vous faites donc vos commits locaux. Mais quelle stratégie adopter lorsque vous faites par exemple un gros refactoring, et que votre copie est totalemment instable tant que vous ne l'avez pas terminé ?
Une solution pratique est de faire un fork de l'origin et de l'ajouter comme second remote, par exemple nommé 'backup'. Vous pouvez ainsi y pousser tous vos commits, toutes vos branches, sans rien polluer sur le remote origin tant que ce n'est pas stable ou proche de l'être. En cas de gros soucis (crash disque, virus violent, ... ) vous avez ainsi toujours une copie à jour de votre travail, mais vous ne compromettez jamais la stabilité du repo origin.



---------------------------------------------------------------------
 
Bibliographie
-------------

### Devenir ami avec GIT

- [A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/), très souvent cité... au début ça paraît idéaliste mais une fois habitué à Git en fait travailler avec beaucoup de branches devient habituel (une fois le premier merge fait pour se rendre compte que les habitudes de précautions prises avec SVN deviennent inutiles, on multiplie facilement les branches)
- [Git for absolute beginners](http://rogerdudler.github.com/git-guide/), très très bonne présentation pour une première prise en main de Git. (Il y a beaucoup d'articles disponibles, celui-ci est vraiment un des meilleurs à ma connaissance pour débuter) 


### Progresser

- [Think like a Git](http://think-like-a-git.net/) Très très complet et avec beaucoup de concepts pour progresser une fois l'outil pris en main, par exemple l'utilisation de branches comme savepoint, ou des patternes de test de merge pour être en confiance dans des environnements cleans)
- [Une Référence Visuelle de Git](http://marklodato.github.io/visual-git-guide/index-fr.html) Bonne représentation française (entre autres) qui permet de bien comprendre ce qui se passe sous le capot et de ne pas se sentir dépassé par le système.	
- [Git interactive Cheat-sheet](http://ndpsoftware.com/git-cheatsheet.html) 



### Avoir des gitignore de pro

GitHub a partagé des collections assez exhaustives de fichiers gitignore. Selon votre configuration, vous aurez parfois à ajuster ça à votre sauce, mais ça reste une mine d'infos pour ne pas réinventer l'eau chaude !
<https://github.com/github/gitignore>


### Logiciels

Les principaux clients sont référencés sur le site officiel. En cas de doute, partez toujours de là !
- [GUIs pour Git](http://git-scm.com/downloads/guis) sur le site officiel de Git



### Pour travailler avec les _modules_ et les _subtrees_ 

Attention : fonctions avanc.es, en principe pas nécessaire avant d'avoir des config un peu spéciale. Ne pas de casser la tête avec au début si vous n'avez pas de besoin d'interconnexions complexes entre des repos.
- [Alternatives To Git Submodule: Git Subtree](http://blogs.atlassian.com/2013/05/alternatives-to-git-submodule-git-subtree/) on Atlassian blog
- [Git Submodules: Core Concept, Workflows And Tips](http://blogs.atlassian.com/2013/03/git-submodules-workflows-tips/) on Atlassian blog
