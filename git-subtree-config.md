Git subtree config Milan
------------------------------------------------------------

2 contents  repos: 

- repo-content-A
- repo-content-B

1 main project:

- repo-project


Dans le repo du projet, créer deux subtree en ajoutant les remotes de contents en tant que remotes
```
    $ git remote add -f contentA https://bitbucket.org/owner/repo-content-A.git
    # On fetch les contenus de la branch develop du remote dans un dossier contents-A/
	$ git subtree add --prefix contents-A contentA develop --squash
	
	$ git remote add -f contentB https://bitbucket.org/owner/repo-content-B.git
    # On fetch les contenus de la branch develop du remote dans un dossier folder-1/proj/contents-B/
	$ git subtree add --prefix folder-1/proj/contents-B contentB develop --squash
```


### Fetching updates from subtree to the main project

Pour mettre à jour depuis le repo Milan :
```
	$ git fetch contentA develop
	$ git subtree pull --prefix contents-A contentA develop --squash
	
	$ git fetch contentB develop
	$ git subtree pull --prefix folder-1/proj/contents-B contentB develop --squash
```

Ou plus facilement, créer un script à la racine avec :
```
	$ echo "Fecthing Contents A for app subtree contents..."
	$ git subtree pull --prefix contents-A contentA develop --squash
	
	$ echo "Fecthing Contents B subtree contents..."
	$ git subtree pull --prefix folder-1/proj/contents-B contentB develop --squash
```
